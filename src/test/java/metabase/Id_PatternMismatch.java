package metabase;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Id_PatternMismatch {
//String URL="https://ldc.lendenclub.com/question/3602";
	String URL="https://ldc.lendenclub.com/question/3658";
WebDriver driver;
public Id_PatternMismatch(WebDriver driver) throws InterruptedException
{
	this.driver=driver;
	((JavascriptExecutor)driver).executeScript("window.open()");
    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
    driver.switchTo().window(tabs.get(1));
    Thread.sleep(3000);
    driver.get(URL);
//    Thread.sleep(10000);
//    driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div[1]/div[2]/a")).click();//click on open editor
//    driver.findElement(By.id("id_sql")).click();
//
//	driver.findElement(By.className("ace_text-input")).sendKeys("select * from account where account_detail_id like '30304%' and type not in ('BORROWER BANK', 'BORROWER CASH FLOW', 'BANK') order by id desc;");
//	driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[1]/div/div[2]/div[2]/button")).click();
    try {
		WebDriverWait wait = new WebDriverWait(driver, 6);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
	} catch (Exception ex) {
		ex.getMessage();
		
	}
    WebElement ele = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[2]"));//find the text field

	String text=ele.getText();
	//System.out.println("first "+ele.getText());       

	//if (text !=null)
	if(text.contains("No results!")){

		System.out.println("No Results Found");
	} else {
		System.out.println("Take Screenshot");


		try {
			// Date format for screenshot file name
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			Date date = new Date();
			String fileName = df.format(date);
			File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			// coppy screenshot file into screenshot folder.
			// FileUtils.copyFile(file, new File(System.getProperty("user.dir") +
			// "./LdcLoanCount/"+fileName+".png"));
			FileUtils.copyFile(file,
					new File("./Resources/" + "ID_Pattern_Mismatch(14)" + " " + "(" + " " + fileName + " " + ")" + ".jpg"));
		} catch (Exception e) {
		}        }
}
}
