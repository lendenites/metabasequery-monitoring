package metabase;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Loan_Account_Details
{
	WebDriver driver;
	String URL="https://ldc.lendenclub.com/question/3589";
public Loan_Account_Details(WebDriver driver) throws InterruptedException
{
	this.driver=driver;
	((JavascriptExecutor)driver).executeScript("window.open()");
    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
    driver.switchTo().window(tabs.get(3));
    Thread.sleep(3000);
    driver.get(URL);
    
    try {
		WebDriverWait wait = new WebDriverWait(driver, 6);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("613f36402")));
	} catch (Exception ex) {
		ex.getMessage();
		
	}
    WebElement ele = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div[2]/div[2]/div[2]"));//find the text field

	String text=ele.getText();
	//System.out.println("first "+ele.getText());       

	//if (text !=null)
	if(text.contains("No results!")){

		System.out.println("No Results Found");
	} else {
		System.out.println("Take Screenshot");


		try {
			// Date format for screenshot file name
			SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
			Date date = new Date();
			String fileName = df.format(date);
			File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			// coppy screenshot file into screenshot folder.
			// FileUtils.copyFile(file, new File(System.getProperty("user.dir") +
			// "./LdcLoanCount/"+fileName+".png"));
			FileUtils.copyFile(file,
					new File("./Resources/" + "Loan_Account_Details(4)" + " " + "(" + " " + fileName + " " + ")" + ".jpg"));
		} catch (Exception e) {
		}
    
    
    
    
    
    
    
//	try {
//		// Date format for screenshot file name
//		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy__hh_mm_ssaa");
//		Date date = new Date();
//		String fileName = df.format(date);
//		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
//		// coppy screenshot file into screenshot folder.
//        //FileUtils.copyFile(file, new File(System.getProperty("user.dir") + "./LdcLoanCount/"+fileName+".png"));
//		FileUtils.copyFile(file, new File("./Resources/"+"Loan_account_Details"+" "+"("+" "+fileName+" "+")"+".jpg"));
//		} catch(Exception e) { }
}
}}

