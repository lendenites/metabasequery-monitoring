package metabaseMain;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import capabilIty.InvestorCashFlow;
import capabilIty.WebCapability;
import metabase.AccountDetailDifferent;
import metabase.AccountDetailDoes;
import metabase.AccountEntryExist2;
import metabase.AccountIdNull;
import metabase.BorrowerCashFlow;
import metabase.Borrower_Account_Detail;
import metabase.Credit_Account_ID;
import metabase.Debit_Account_ID;
import metabase.InvestorBankMismatch;
import metabase.Id_PatternMismatch;
import metabase.InvDoesNotExist;
import metabase.Investment_Flevin;
import metabase.InvestoerCashFlow1;
import metabase.Investor_Account_Details;
import metabase.LOCDOesNotExist;
import metabase.LOC_Account_Detail;
import metabase.Line_Of_Credit;
import metabase.LoanAccountdoesnotexist_AT;
import metabase.Loan_Account_Details;
import metabase.MultipleAccountEntry;
import metabase.BorrowerLOC;
import metabase.BorrowerLoan;
import metabase.InvestorDetailMismatch;
import metabase.BorrowerDetailMismatch;

public class MainClass extends WebCapability
{
	WebDriver driver;



	@BeforeTest
	public void openchrome()
	{
		driver = WebCapability();
		driver.manage().window().maximize();
	}
	@Test(priority = 1)
	public void query01() throws InterruptedException
	{
		new Credit_Account_ID(driver);

	}
	@Test(priority = 2)
	public void Debit02() throws InterruptedException
	{
		new Debit_Account_ID(driver);
	}
	@Test(priority = 3)
	public void borrower03() throws Exception
	{
		new Borrower_Account_Detail(driver);
	}
	@Test(priority = 4)
	public void Loan04() throws InterruptedException
	{
		new Loan_Account_Details(driver);
	}
	@Test(priority = 5)
	public void LOC05() throws InterruptedException
	{
		new Line_Of_Credit(driver);
	}
	@Test(priority = 6)
	public void Investor06() throws InterruptedException
	{
		new Investor_Account_Details(driver);
	}
	@Test(priority = 7)
	public void loan07() throws InterruptedException
	{
		new LoanAccountdoesnotexist_AT(driver);
	}
	@Test(priority = 8)
	public void Loc08() throws InterruptedException
	{
		new LOC_Account_Detail(driver);
	}
	@Test(priority = 9)
	public void LOCDOesNotExist009() throws InterruptedException
	{
		new LOCDOesNotExist(driver);
	}
	@Test(priority = 12)
	public void type11() throws InterruptedException
	{
		new InvestorDetailMismatch(driver);
	}
	@Test(priority = 13)
	public void type12() throws InterruptedException
	{
		new BorrowerDetailMismatch(driver);
	}
	@Test(priority = 14)
	public void type13() throws InterruptedException
	{
		new BorrowerLOC(driver);
	}
	@Test(priority = 15)
	public void type14() throws InterruptedException
	{
		new BorrowerLoan(driver);
	}
	@Test(priority = 16)
	public void Id_Pattern15() throws InterruptedException
	{
		new Id_PatternMismatch(driver);
	}
	@Test(priority = 17)
	public void id16() throws InterruptedException
	{
		new InvestorBankMismatch(driver);
	}
	@Test(priority = 18)
	public void AccDoes17() throws InterruptedException
	{
		new AccountDetailDoes(driver);
	}
	@Test(priority = 19)
	public void Accexist() throws InterruptedException
	{
		new AccountEntryExist2(driver);
	}
	@Test(priority = 20)
	public void IDNull() throws InterruptedException
	{
		new AccountIdNull(driver);
	}
	@Test(priority = 21)
	public void BorrowerBankAccount() throws InterruptedException
	{
		new metabase.BorrowerBankAccount(driver);
	}
	@Test(priority = 22)
	public void BorrwerCashFlow() throws InterruptedException
	{
		new BorrowerCashFlow(driver);
	}
	@Test(priority = 23)
	public void Investorcashflow() throws InterruptedException
	{
		new InvestorCashFlow(driver);
	}
	@Test(priority = 24)
	public void Invcf1() throws InterruptedException
	{
		new InvestoerCashFlow1(driver);
	}
	@Test(priority = 25)
	public void Diff() throws InterruptedException
	{
		new AccountDetailDifferent(driver);
	}
	@Test(priority = 26)
	public void MultipleEntry() throws InterruptedException
	{
		new MultipleAccountEntry(driver);
	}
	@Test(priority = 11)
	public void Invdne10() throws InterruptedException
	{
		new InvDoesNotExist(driver);
	}

	@AfterTest
	public void CloseBrowser()
	{

		driver.quit	();
	}

}










